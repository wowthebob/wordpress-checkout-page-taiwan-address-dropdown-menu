alert('123');

jQuery(function ($) {

    if (!$('form.woocommerce-checkout')) {
        return;
    }

    var cities = Inno_Object.cities;


    var target_checkout_fields = [
        {
            state: '#billing_state',
            city: '#billing_city',
            postcode: '#billing_postcode'}
        , {
            state: '#shipping_state',
            city: '#shipping_city',
            postcode: '#shipping_postcode'
        }
    ];

    /**
     * init all state fields, on state change, load city list
     */
    target_checkout_fields.forEach(function (field) {

        var state_field = field.state;

        var city_field = field.city;

        $(state_field).change(function () {

            //get current city
            var current_city = $(city_field).val();

            var state = $(this).val();

            var has_current_city = false;

            //destroy select2 first

            if ($(city_field).hasClass("select2-hidden-accessible")) {
                $(city_field).select2('destroy');
            }

            if (state && cities[state]) {

                var state_cities = cities[state];

                var data = [];

                state_cities.forEach(function (city) {
                    data.push({
                        id: city.suburb,
                        text: city.suburb
                    });

                    if (city.suburb == current_city) {
                        has_current_city = true;
                    }
                });

                $(city_field).select2({
                    data: data,
                    placeholder: "選擇鄉鎮市",
                    width:'100%'
                });
            }

            if (!has_current_city) {
                $(city_field).val('');
            }

            $(city_field).trigger('change');

        });

        $(state_field).trigger('change');
    });


    /**
     * On city change, load post code automatically
     */
    target_checkout_fields.forEach(function (field) {

        var state_field = field.state;

        var city_field = field.city;

        var postcode_field = field.postcode;



        $(city_field).change(function () {

            //reset
            var state = $(state_field).val();

            var selected_city = $(this).val();

            var reset_postcode = true;

            if (state && cities[state]) {

                var state_cities = cities[state];

                state_cities.forEach(function (city) {

                    if (city.suburb == selected_city) {

                        $(postcode_field).val(city.postcode);

                        reset_postcode = false;
                    }

                });

            }

            if (reset_postcode) {

                $(postcode_field).val('');
            }

        });

        $(city_field).trigger('change');

    });

});
